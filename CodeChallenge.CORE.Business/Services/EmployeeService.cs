﻿using System.Collections.Generic;
using CodeChallenge.CORE.Business.Contract;
using CodeChallenge.CORE.Business.Dto;
using CodeChallenge.CORE.Business.Entities;
using CodeChallenge.CORE.Data.Repositories;

namespace CodeChallenge.CORE.Business.Services
{
    public class EmployeeService : IEmployeeData
    {
        EmployeeRepository repository = new EmployeeRepository();

        public IEnumerable<EmployeeDTO> GetAll()
        {
            List<EmployeeDTO> results = new List<EmployeeDTO>();
            var allEmployees = repository.GetAll();

            foreach (var item in allEmployees)
            {

                if (item.Contract.ContractType.Equals(CodeChallenge.CORE.Data.Models.ContractType.Hourly))
                {
                    results.Add(new HourlyContractEmployee(item));
                }
                else
                {
                    results.Add(new MonthlyContractEmployee(item));
                }
            }
            return results;
        } 

        public EmployeeDTO Get(int id)
        {
           var item =  repository.GetById(id);
            if (item.Contract.ContractType.Equals(CodeChallenge.CORE.Data.Models.ContractType.Hourly))
            {
                return new HourlyContractEmployee(item);
            } 

            return new MonthlyContractEmployee(item);
        }
    }
}
