﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeChallenge.CORE.Business.Dto;

namespace CodeChallenge.CORE.Business.Contract
{
    public interface IEmployeeData
    {
        IEnumerable<EmployeeDTO> GetAll();
        EmployeeDTO Get(int id);
    }
}
