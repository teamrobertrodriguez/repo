﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeChallenge.CORE.Business.Contract;
using CodeChallenge.CORE.Business.Dto;

namespace CodeChallenge.CORE.Business.Entities
{
    public abstract class Employee  : EmployeeDTO
    { 
        protected CodeChallenge.CORE.Data.Models.Employee _employeeModel;

        public Employee(CodeChallenge.CORE.Data.Models.Employee employeeModel)
        {
            _employeeModel = employeeModel;
            this.Id = employeeModel.Id;
            this.Name = employeeModel.Name;
            this.ContractTypeName = employeeModel.Contract.Name;
            this.RoleId = employeeModel.Role.Id;
            this.RoleName = employeeModel.Role.Name;
            this.RoleDescription = employeeModel.Role.Description ?? "";
            this.AnnualSalary = this.CalculateAnnualSalary();
        }
        public abstract decimal CalculateAnnualSalary();
    }

    public class HourlyContractEmployee : Employee
    {
        public sealed override decimal CalculateAnnualSalary()
        {
            return 120 * this._employeeModel.HourlySalary * 12;
        }

        public HourlyContractEmployee(Data.Models.Employee employeeModel) : base(employeeModel)
        {
        }
    }

    public class MonthlyContractEmployee : Employee
    {
        public sealed override decimal CalculateAnnualSalary()
        {
            return this._employeeModel.MonthlySalary * 12;
        }

        public MonthlyContractEmployee(Data.Models.Employee employeeModel) : base(employeeModel)
        {
        }
    }
}
