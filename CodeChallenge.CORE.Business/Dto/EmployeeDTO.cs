﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeChallenge.CORE.Business.Dto
{
    public class EmployeeDTO
    {
        public int Id;
        public string Name;
        public string ContractTypeName;
        public int RoleId;
        public string RoleName;
        public string RoleDescription;
        public decimal AnnualSalary;
    }
}
