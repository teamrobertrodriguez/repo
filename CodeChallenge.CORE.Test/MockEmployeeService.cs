﻿using System.Collections.Generic; 
using CodeChallenge.CORE.Business.Contract;
using CodeChallenge.CORE.Business.Dto;

namespace CodeChallenge.CORE.Test
{
    public class MockEmployeeService : IEmployeeData
    {
        public IEnumerable<EmployeeDTO> GetAll()
        {
            var results = new List<EmployeeDTO>();

            results.Add(new EmployeeDTO()
            {
                Id = 1,
                Name = "Robert",
                AnnualSalary = 100000,
                ContractTypeName = "Contract",
                RoleName = "Administrator",
                RoleId = 1,
                RoleDescription = "Boss"
            });

            results.Add(new EmployeeDTO()
            {
                Id = 1,
                Name = "Eithan",
                AnnualSalary = 120000,
                ContractTypeName = "Contract",
                RoleName = "Employee",
                RoleId = 1,
                RoleDescription = "Employee"
            });

            return results;
        }

        public EmployeeDTO Get(int id)
        {
            return new EmployeeDTO()
            {
               Id = 1,
               Name = "Robert",
               AnnualSalary = 100000,
               ContractTypeName = "Contract",
               RoleName = "Administrator",
               RoleId = 1,
               RoleDescription = "Boss"
            };
        }
    }
}
