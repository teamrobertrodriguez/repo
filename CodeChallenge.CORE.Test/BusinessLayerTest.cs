using System.Collections;
using System.Collections.Generic;
using CodeChallenge.CORE.Business.Contract;
using CodeChallenge.CORE.Business.Dto;
using CodeChallenge.CORE.Business.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CodeChallenge.CORE.Test
{
   

    [TestClass]
    public class BusinessLayerTest
    {
        private MockEmployeeService _employeeService;

        [TestInitialize]
        public void Init()
        {
            _employeeService = new MockEmployeeService();
        }
        [TestMethod]
        public void TestGetAll()
        {
            var data = _employeeService.GetAll();
            CollectionAssert.AllItemsAreUnique( data as ICollection);
        }

        [TestMethod]
        public void TestGetById()
        {
            var data = _employeeService.Get(1);
            Assert.AreEqual(data.Id , 1);
        }
    }
}
