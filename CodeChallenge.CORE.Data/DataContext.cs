﻿using System; 
using System.Collections.Generic;
using System.Linq;
using System.Reflection; 
using System.Threading.Tasks;
using CodeChallenge.CORE.Data.Models;
using CodeChallenge.CORE.Data.WebServices;

namespace CodeChallenge.CORE.Data
{
    internal class DataContext
    {
        private static readonly DataContext _instance = new DataContext();

        public static DataContext Instance
        {
            get { return _instance; }
        }
         
        internal T GetById<T>(int id)
        {
            var list = GetData<T>();

            var rawResults = (from n in list
                where n.Id.Equals(id)
                select n).FirstOrDefault();
            
            var returnInstance = MapResults<T>(rawResults);

            return (T)returnInstance;
        }

        internal ICollection<T> GetAll<T>()
        {
            ICollection<T> returnList = new List<T>();
            var list = GetData<T>();

            foreach (var item in list)
            {
                returnList.Add(this.GetById<T>(item.Id));
            }
            return (ICollection<T> )returnList;
        }

        private object MapResults<T>(EmployeeServiceDTO rawResults)
        {
            var returnInstance = Activator.CreateInstance(typeof(T)) as Employee;

            returnInstance.Id = rawResults.Id;
            returnInstance.Name = rawResults.Name;
            returnInstance.Role = new Role()
            {
                Id = rawResults.RoleId,
                Name = rawResults.RoleName,
                Description = rawResults.RoleDescription
            };
            returnInstance.Contract = new Contract()
            {
                Name = rawResults.ContractTypeName,
                ContractType = (rawResults.ContractTypeName.Contains("Hourly") ? ContractType.Hourly : ContractType.Monthly)
            };
            returnInstance.HourlySalary = rawResults.HourlySalary;
            returnInstance.MonthlySalary = rawResults.MonthlySalary;
            return returnInstance;
        }

        private List<EmployeeServiceDTO> GetData<T>()
        {
            Type type = Type.GetType("CodeChallenge.CORE.Data.WebServices." + typeof(T).Name + "Service");
            var serviceInstance = Activator.CreateInstance(type);
            Type serviceInstanceType = serviceInstance.GetType();
            MethodInfo methodInfo = serviceInstanceType.GetMethod("GetAsync");

            var task = (Task)methodInfo.Invoke(serviceInstance, null);
            task.Wait();

            //no time for dynamic Linq queries.. but that was my Idea. :)
            //that's why I am hardcoding the types
            var list = ((List<EmployeeServiceDTO>)task.GetType().GetProperty("Result").GetValue(task))
                .Cast<EmployeeServiceDTO>().ToList();
            return list;
        }
    }
}
