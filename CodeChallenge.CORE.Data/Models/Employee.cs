﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeChallenge.CORE.Data.Models
{
    public class Employee : BaseModel
    {
        public Contract Contract { get; set; }
        public Role Role { get; set; }
        public  decimal HourlySalary { get; set; }
        public decimal MonthlySalary { get; set; }
    }
}
