﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeChallenge.CORE.Data.Models
{
    public enum ContractType
    {
        Hourly,
        Monthly
    }
    public class Contract : BaseModel
    {
        public ContractType ContractType { get; set; }
    }
}
