﻿using System.Net.Http;
using System.Threading.Tasks;

namespace CodeChallenge.CORE.Data.WebServices
{
    public class BaseService<TResult>
    {
        private string Url { get; set; }

        public BaseService(string ApiUrl)
        {
            this.Url = ApiUrl;
        }

        public async Task<TResult> GetAsync()
        {
            using (var httpClient = new HttpClient())
            using (var httpResponse = await httpClient.GetAsync(Url).ConfigureAwait(false))
            {
                return await httpResponse.Content.ReadAsAsync<TResult>();
            }
        }
    }
}
