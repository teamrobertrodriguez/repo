﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeChallenge.CORE.Data.WebServices
{
    public static class ServicesInfo
    {
        public static string EmployeeApiUrl { get;} = "http://masglobaltestapi.azurewebsites.net/api/Employees";
    }
}
