﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace CodeChallenge.CORE.Data.WebServices
{

    public class EmployeeService : BaseService<List<EmployeeServiceDTO>>
    {
        public EmployeeService() : base(ServicesInfo.EmployeeApiUrl)
        {
        }
    }
}
