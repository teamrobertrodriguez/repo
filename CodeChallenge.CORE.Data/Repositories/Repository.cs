﻿using System;
using System.Collections.Generic;

namespace CodeChallenge.CORE.Data.Repositories
{

    public class Repository<T> : IRepository<T>
        where T : class
    {
        private DataContext _context;

        public Repository()
        {
            _context = DataContext.Instance;
        }

        public void Add(T entity)
        {
            throw new NotImplementedException();
        }

        public void Update(T entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(T entity)
        {
            throw new NotImplementedException();
        }

        public ICollection<T> GetAll()
        {
            return _context.GetAll<T>();
        }
        public T GetById(int id)
        {
            return _context.GetById<T>(id);
        }
    }

    public interface IRepository<T>
    {
        void Add(T entity);
        void Update(T entity);
        void Delete(T entity);
        T GetById(int id);
        ICollection<T> GetAll();
    }
}
