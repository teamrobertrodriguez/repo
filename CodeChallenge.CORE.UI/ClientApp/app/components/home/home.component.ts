import { Component, Inject } from '@angular/core';
import { Http } from '@angular/http';

@Component({
    selector: 'home',
    templateUrl: './home.component.html'
})
export class HomeComponent {
    public employees: Employees[];
    public loader: boolean;
    private _http: Http;
    private _baseUrl: string;
    
    constructor(http: Http, @Inject('BASE_URL') baseUrl: string) {
        this._http = http;
        this._baseUrl = baseUrl;
        this.loader = false;
    }

    public getById(id: HTMLInputElement) {
        this.loader = true;
        this.employees = [];
        const isSearch = id.value != "" && id.value !== "0";
        const apiUrl = isSearch
            ? this._baseUrl + 'api/Employee/Get/' + id.value
            : this._baseUrl + 'api/Employee/GetAll';

        this._http.get(apiUrl).subscribe(result => {
            if (isSearch) {
                this.employees.push(result.json() as Employees);
            } else {
                this.employees = result.json() as Employees[];
            }
            this.loader = false;
        }, error => {
            this.loader = false;
            console.error(error);
        });
    }
}

export interface Employees {
    id: string;
    name: string;
    contractTypeName: string;
    roleId: number;
    roleName: string;
    roleDescription: string;
    annualSalary: number;
}

