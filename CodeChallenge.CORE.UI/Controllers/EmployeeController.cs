using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using CodeChallenge.CORE.Business.Contract;
using CodeChallenge.CORE.Business.Dto;
using CodeChallenge.CORE.Business.Services;
using Microsoft.AspNetCore.Mvc;

namespace CodeChallenge.CORE.UI.Controllers
{
    [Route("api/[controller]/[action]")]
    public class EmployeeController : Controller
    {
        private IEmployeeData _employeeService;
        public EmployeeController(IEmployeeData employeeService)
        {
            _employeeService = employeeService;
        }

        public IActionResult GetAll()
        {
            IEnumerable<EmployeeDTO> result;
            try
            {
                result = _employeeService.GetAll();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Ok(result);
        }
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            EmployeeDTO result;
            try
            {
                result = _employeeService.Get(id);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Ok(result);
        }
    }
}
